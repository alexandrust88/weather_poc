from requests.adapters import HTTPAdapter
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests.packages.urllib3.util.retry import Retry
import requests
import json
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class Weather():

    def __init__( self ):
        self.sess = requests.Session()
        self.retry = Retry(connect=3, backoff_factor=0.5)
        self.adapter = HTTPAdapter(max_retries=self.retry)
        self.sess.mount('https://', self.adapter)
        self.userpass = (None, None)
        self.sess.auth = self.userpass
        self.schema = 'https://'
        self._url = 'www.weerindelft.nl/clientraw.txt'  ### client raw txt contains all the current weather information , no need for something else
        self.url = self.schema + self._url

    def fetchTemperatureEndpoint( self ):
        try:
            r=(self.sess.get(self.url,headers={"content-type": "application/json"},verify=False))  ## it is already formatted in celsius because of  https://saratoga-weather.org/wdparser/index.php?site=https%3A%2F%2Fwww.weerindelft.nl&submit=Submit#clientraw.txt
            print( r.text.split(" ") )
            print("{} degrees Celsius ".format(r.text.split(" ")[4]))
        except Exception as e :
            print("Exception occured ,please try again later {}".format(e))
            print("{} degrees Celsius ".format(r))

if __name__ == "__main__":
    obj=Weather()
    obj.fetchTemperatureEndpoint()