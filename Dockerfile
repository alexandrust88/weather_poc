FROM python:3.7
WORKDIR /app
COPY ./app/requirements.txt .
RUN pip install -r requirements.txt
COPY ./app/src/ .
CMD [ "python", "./HoeWarmIsHetInDelft.py" ]